# Installation guide

## Install dependencies

Dependencies are stored in [requirements.txt](requirements.txt)

sudo apt install $(cat requirements.txt)

## Execution

The content is made to be served by Jekyll

```
jekyll serve --incremental
```

The website is now accessible at [http://localhost:4000](http://localhost:4000)
