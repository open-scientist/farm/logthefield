---
title: Tableau de bord de la ferme
layout: default
---


<div id="dashboard"></div>

<script type="text/javascript">
  p = document.createElement('p');
  actions_list = localStorage.getItem('action');
  p.innerText = "Actions récemment réalisées sur la ferme : " + actions_list;
  dashboard = document.getElementById('dashboard');
  dashboard.append(p);
</script>

